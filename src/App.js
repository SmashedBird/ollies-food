import React from 'react'
import styles from './App.module.scss'
import Header from './containers/Header'
import Footer from './containers/Footer'
import SignUp from './containers/SignUp'
import Img from './components/Img'

function App() {
  return (
    <div className='App'>
      <Header />
      <section className={styles.lightBackground}>
        <div className={styles.signUp}>
          <div>
            <Img
              className={styles.heroImage}
              src='https://via.placeholder.com/600'
              alt="Ollie's Placeholder Image"
            />
          </div>
          <SignUp />
        </div>
      </section>
      <Footer />
    </div>
  )
}

export default App
