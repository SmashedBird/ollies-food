import React from 'react'
import styles from './Header.module.scss'

function App() {
  return (
    <header className={styles.nav}>
      <div className={styles.navLogo}>Ollie&apos;s Food</div>
    </header>
  )
}

export default App
