import React from 'react'
import styles from './Footer.module.scss'

function App() {
  return (
    <footer className={styles.footer}>
      <h4 className={styles.header}>Contact</h4>
      <p>Bastien Buchaca</p>
      <p>
        <a href='mailto:Bastien@smashedbird.com?subject=Next step for the interview'>
          Bastien@smashedbird.com
        </a>
      </p>
      <p>
        <a href='tel:512-923-5309'>512-923-5309</a>
      </p>
    </footer>
  )
}

export default App
