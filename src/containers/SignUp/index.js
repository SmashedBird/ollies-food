import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FieldArray, reduxForm } from 'redux-form'
import { createAccount } from 'actions/signUp'
import Input from 'components/form/Input'
import Button from 'components/Button'

import styles from './SignUp.module.scss'
import Pets from './Pets'
import {
  required,
  email,
  minLength8,
  minDigit1,
  minLetter1,
  minSpecialChar1,
  passwordsMatch,
} from 'tools/validators'

const SignUp = (props) => {
  const { handleSubmit, dispatch } = props
  return (
    <div className={styles.signUpForm}>
      <form onSubmit={handleSubmit((val) => dispatch(createAccount(val)))}>
        <Input
          label='Email'
          name='email'
          type='text'
          validate={[required, email]}
        />
        <Input
          label='Password'
          name='password'
          type='password'
          validate={[
            required,
            minLength8,
            minDigit1,
            minLetter1,
            minSpecialChar1,
          ]}
        />
        <Input
          label='Confirm Password'
          name='passwordConfirm'
          type='password'
          validate={[required, passwordsMatch]}
        />
        <FieldArray name='pets' component={Pets} />
        <div>
          <Button type='submit' name='Submit' className={styles.formCTA}>
            Submit
          </Button>
        </div>
      </form>
    </div>
  )
}

SignUp.propTypes = {
  handleSubmit: PropTypes.func,
  dispatch: PropTypes.func,
  state: PropTypes.object,
}

let InitializeFromStateForm = reduxForm({
  form: 'signUp',
  enableReinitialize: true,
})(SignUp)

InitializeFromStateForm = connect(() => ({
  initialValues: { pets: [{}] },
}))(InitializeFromStateForm)

export default InitializeFromStateForm
