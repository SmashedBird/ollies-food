import React from 'react'
import PropTypes from 'prop-types'
import Input from 'components/form/Input'
import AddRowContainer from 'containers/AddRow/AddRowContainer'
import RemoveRowContainer from 'containers/RemoveRow/RemoveRowContainer'
import styles from './../SignUp.module.scss'
import { required, maxLength10, moreThan2, max150 } from 'tools/validators'

const Pets = ({ fields }) => (
  <>
    <div className={styles.petFields}>
      {fields.map((field, index) => (
        <div className={styles.petFieldsRow} key={index}>
          <Input
            label='Pet Name'
            name={`${field}.petName`}
            type='text'
            validate={[required, maxLength10]}
          />
          <Input
            label='Weight'
            name={`${field}.petWeight`}
            type='number'
            validate={[required, moreThan2, max150]}
          />
          <Input
            label='Ideal Weight'
            name={`${field}.petWeightIdeal`}
            type='number'
            validate={[moreThan2, max150]}
          />
          {fields.length > 1 && (
            <div>
              <RemoveRowContainer
                formKey='signUp'
                fieldKey={fields.name}
                itemKey={index}
              />
            </div>
          )}
        </div>
      ))}
    </div>
    <AddRowContainer
      title='pet'
      formKey='signUp'
      fieldKey={fields.name}
      initializeValue={{
        petName: '',
        petWeight: '',
        petWeightIdeal: '',
      }}
      disabledMessage='We will let you add the rest of the family later in the process'
      disabled={fields.length >= 3}
    />
  </>
)
Pets.propTypes = {
  fields: PropTypes.object.isRequired,
  isSectionIncluded: PropTypes.bool,
}
export default Pets
