import { connect } from 'react-redux'
import { arrayPush } from 'redux-form'

import AddRow from 'components/AddRow'

const mapDispatchToProps = (
  dispatch,
  { formKey, fieldKey, initializeValue }
) => {
  return {
    addRow: () => dispatch(arrayPush(formKey, fieldKey, initializeValue)),
  }
}

export default connect(null, mapDispatchToProps)(AddRow)
