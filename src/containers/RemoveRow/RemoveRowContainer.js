import { connect } from 'react-redux'
import { arrayRemove } from 'redux-form'

import RemoveRow from 'components/RemoveRow'

const mapDispatchToProps = (dispatch, { formKey, fieldKey, itemKey }) => {
  return {
    onRemove: () => dispatch(arrayRemove(formKey, fieldKey, itemKey)),
  }
}

export default connect(null, mapDispatchToProps)(RemoveRow)
