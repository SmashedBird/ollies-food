// for validation function used with redux-forms, if you return undefined that
// means the validation passed, if you return a string, then that validation is broken and that is the error message
import { isEmpty, isNumber } from './objectHelper'

export const required = (value) => (isEmpty(value) ? 'Required' : undefined)

export const moreThan2 = (num) => moreThan(2)(num)
export const max150 = (num) => lessThanEqualTo(150)(num)

export const minLength8 = (length) => minLength(8)(length)

export const minDigit1 = (num) => minMumberOfDigit(1)(num)
export const minLetter1 = (num) => minMumberOfLetter(1)(num)
export const minSpecialChar1 = (num) => minMumberOfSpecialChar(1)(num)

export const maxLength10 = (length) => maxLength(10)(length)

export const passwordsMatch = (value, allValues) =>
  value !== allValues.password ? "Your passwords don't match" : undefined

export const email = (value) =>
  value && /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(value)
    ? undefined
    : 'Invalid email address'

export const numbersOnly = (value) =>
  value && /[^0-9]/i.test(value) ? 'Number characters only' : undefined

export const lessThan = (max) => (value) => {
  const areNumbers = isNumber(max) && isNumber(value)
  if (!areNumbers) return undefined
  return parseFloat(value) < parseFloat(max)
    ? undefined
    : `Field Must be less than ${max}`
}

export const lessThanEqualTo = (max) => (value) => {
  const areNumbers = isNumber(max) && isNumber(value)
  if (!areNumbers) return undefined
  return parseFloat(value) <= parseFloat(max)
    ? undefined
    : `Field Must be under ${max}`
}

export const moreThan = (min) => (value) => {
  const areNumbers = isNumber(min) && isNumber(value)
  if (!areNumbers) return undefined
  return parseFloat(value) > parseFloat(min)
    ? undefined
    : `Field Must be more than ${min}`
}

export const moreThanEqualTo = (min) => (value) => {
  const areNumbers = isNumber(min) && isNumber(value)
  if (!areNumbers) return undefined
  return parseFloat(value) >= parseFloat(min)
    ? undefined
    : `Field Must be over ${min}`
}

export const minLength = (minLength) => (value) => {
  if (!value) return undefined
  return String(value).length >= minLength
    ? undefined
    : `Minimum length allowed is ${minLength} characters.`
}

export const maxLength = (maxLength) => (value) => {
  if (!value) return undefined
  return String(value).length <= maxLength
    ? undefined
    : `Maximum length allowed is ${maxLength} character${
        maxLength > 1 ? 's' : ''
      }.`
}

// VALUE TYPE VALIDATIONS

export const alphaNumeric = (value) =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Only alphanumeric characters'
    : undefined

export const alphaOnly = (value) =>
  value && /[^a-zA-Z ]/i.test(value) ? 'Only alpha characters' : undefined

export const noDecimals = (value) =>
  value && /[,.]/i.test(value) ? 'No Decimals allowed' : undefined

export const minMumberOfDigit = (minNumb) => (value) => {
  if (!value) return undefined
  return value.replace(/[^0-9]/g, '').length >= minNumb
    ? undefined
    : `Minimum ${minNumb} digit${minNumb > 1 ? 's' : ''} required.`
}

export const minMumberOfLetter = (minNumb) => (value) => {
  if (!value) return undefined
  return value.replace(/[^a-zA-Z]/g, '').length >= minNumb
    ? undefined
    : `Minimum ${minNumb} letter character${minNumb > 1 ? 's' : ''} required.`
}

export const minMumberOfSpecialChar = (minNumb) => (value) => {
  if (!value) return undefined
  return value.length -
    value.replace(/[~`!@#$%^&*(){}[\];:"'<,.>?/\\|_+=-]/g, '').length >=
    minNumb
    ? undefined
    : `Minimum ${minNumb} special character${minNumb > 1 ? 's' : ''} required.`
}
