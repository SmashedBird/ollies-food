import { SubmissionError } from 'redux-form'

const throwException = (error) => {
  if (error.status === 403) {
    console.log('ERROR, GO TO LOGIN')
  }
}

const request = (path, method, body, withHeaders = false, headers = {}) => {
  const reqHeaders = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    ...headers,
  }

  return new Promise((resolve, reject) => {
    const responseHeaders = {}
    fetch(path, {
      method: method || 'GET',
      headers: reqHeaders,
      body: body && JSON.stringify(body),
    })
      .then((response) => {
        if (response.status >= 200 && response.status < 400) {
          return response
        }
        const error = new Error(response.statusText)
        error.response = response
        throw error
      })
      .then((response) => {
        if (response.headers.entries) {
          for (var pair of response.headers.entries()) {
            responseHeaders[pair[0]] = pair[1]
          }
        }
        return response.status !== 204 && response.json()
      })
      .then((parsedResponse) => {
        if (withHeaders) {
          resolve({
            response: parsedResponse,
            headers: responseHeaders,
          })

          return
        }
        resolve(parsedResponse)
      })
      .catch(async (err) => {
        if (err.response) {
          try {
            const parsedError = await err.response.json()
            if (parsedError.message) {
              // SUPER HACKY BUT I WOULD RECOMMEND THE BACKEND TO RETURN THE FIELDS FACING AN ERROR AS :
              // {
              //   email: 'Email already taken',
              //   ...
              // }
              // SO WE CAN LEVERAGE REDUX FORM ERROR HANDLING INSTEAD OF USING THE COUPLE LINES BELOW
              let error = {}
              if (new RegExp('^email', 'i').test(parsedError.message))
                Object.assign(error, { email: parsedError.message })
              // Throw a field error
              throw new SubmissionError(error)
            }
            throwException(parsedError)
            return reject(parsedError)
          } catch (e) {
            // Will throw if the backend responed and the response has no data (responses like 404)
            return reject(e)
          }
        }

        // happens for timeout exceptions
        throwException([{ message: err }])
        reject()
      })
  })
}

export default request
