import React from 'react'
import PropTypes from 'prop-types'

import styles from './AddRow.module.scss'
import AddIcon from '@material-ui/icons/Add'

import Button from 'components/Button'

const AddRow = ({ title, addRow, disabled, disabledMessage, className }) => {
  return (
    <Button
      type='button'
      onClick={addRow}
      className={`${styles.addRow} ${className} ${
        disabled ? styles.addRowDisabled : ''
      }`}
      classNameDisabled={styles.addRowDisabled}
      disabled={disabled}
    >
      {!disabled && <AddIcon className={styles.addRowIcon} />}
      <span
        className={`${
          disabled ? styles.addDisabledRowDescription : styles.addRowDescription
        }`}
      >
        {disabled
          ? disabledMessage || `No more ${title.toLowerCase()}s can be added.`
          : `Add another ${title}`}
      </span>
    </Button>
  )
}

AddRow.propTypes = {
  title: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  disabledMessage: PropTypes.string,
  addRow: PropTypes.func.isRequired,
  className: PropTypes.string,
}

export default AddRow
