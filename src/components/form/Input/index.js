import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import styles from './Input.module.scss'

const renderField = ({
  input,
  placeholder,
  type,
  meta: { touched, error, warning },
}) => (
  <>
    <div>
      <input
        className={styles.formInput}
        {...input}
        placeholder={placeholder}
        type={type}
      />
    </div>
    <div className={styles.inputError}>
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </>
)

const Input = (props) => {
  const { label, name, type, placeholder, validate } = props

  return (
    <div className={styles.inputWrapper}>
      <label className={styles.fieldLabel}>{label}</label>
      <Field
        name={name}
        component={renderField}
        type={type}
        placeholder={placeholder}
        validate={validate}
      />
    </div>
  )
}

Input.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.oneOf(['text', 'number', 'password']).isRequired,
  placeholder: PropTypes.string,
  validate: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.arrayOf(PropTypes.func),
  ]),
}

export default Input
