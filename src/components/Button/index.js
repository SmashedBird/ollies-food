import React from 'react'
import PropTypes from 'prop-types'
import styles from './Button.module.scss'

const Button = ({
  children,
  onClick,
  className,
  classNameDisabled,
  type = 'button',
  disabled,
}) => {
  return (
    <button
      type={type}
      className={`
          ${styles.button} 
          ${className || ''}
          ${disabled && classNameDisabled ? classNameDisabled : ''}
        `}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  path: PropTypes.string,
  className: PropTypes.string,
  classNameDisabled: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.oneOf(['button', 'link', 'submit']),
  disabled: PropTypes.bool,
}

export default Button
