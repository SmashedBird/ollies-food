import React from 'react'
import PropTypes from 'prop-types'

import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'

const RemoveRow = ({ disabled, onRemove, className }) => {
  return disabled ? null : (
    <IconButton onClick={onRemove} className={className}>
      <Icon>clear</Icon>
    </IconButton>
  )
}

RemoveRow.propTypes = {
  onRemove: PropTypes.func.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
}

export default RemoveRow
