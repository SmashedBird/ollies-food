import request from 'tools/request'
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS'

export const signUpSuccess = (data) => ({
  type: SIGN_UP_SUCCESS,
  data,
})

export const createAccount = (formData) => async (dispatch) => {
  const response = await request(
    'https://32f2jzoot4.execute-api.us-east-1.amazonaws.com/default/fe-takehome-api',
    'POST',
    formData
  )
  console.log('Success, Router push to next page', formData)
  dispatch(signUpSuccess(response))
  return response
}
